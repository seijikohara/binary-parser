package utils.binary.parser;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import utils.binary.dto.Res;
import utils.binary.dto.chunk.ResChunk;
import utils.binary.exception.UnsupportedChunkTypeException;

public class ResParser extends BinaryDataParser<Res> {

	public ResParser(byte[] data) {
		super(data);
	}

	public ResParser(ByteBuffer data) {
		super(data);
	}

	static final String RES_CHUNK_CODE = "res ";

	public Res parse() {
		ResChunk resChunk = new ResChunk();
		nextChunk();
		if (!RES_CHUNK_CODE.equals(getCurrentChunkType())) {
			throw new UnsupportedChunkTypeException(getCurrentChunkType(),
					getPosition());
		}
		populateCurrentChunkHeader(resChunk);

		List<ResChunk.ResData> resDatas = new ArrayList<ResChunk.ResData>();
		do {
			ResChunk.ResData resData = new ResChunk.ResData();
			resData.setInvalidFlag(readBoolean());
			resData.setGpsLatitude(readDouble());
			resData.setGpsLongitude(readDouble());
			resData.setGpsBankAngle(readFloat());
			resData.setGpsAcceleration(readFloat());
			resDatas.add(resData);
		} while (hasRemaining());
		resChunk.setResDatas(resDatas);

		Res res = new Res();
		res.setResChunk(resChunk);
		return res;
	}
}
