package utils.binary.parser;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import org.apache.commons.lang3.StringUtils;

import utils.binary.dto.Dto;
import utils.binary.dto.chunk.Chunk;

/**
 * バイナリフォーマットからDTOへデータ変換を担う基底クラス
 *
 * @author kohara
 *
 * @param <T>
 *            DTO
 */
public abstract class BinaryDataParser<T extends Dto> {

	public BinaryDataParser(ByteBuffer data) {
		this.buffer = data;
	}

	public BinaryDataParser(byte[] data) {
		this(ByteBuffer.wrap(data));
	}

	private final ByteBuffer buffer;
	private String currentChunkType;
	private int currentChunkSize;
	private int chunkStartPosition;
	private Charset charset = Charset.forName("UTF-8");

	/**
	 * バイナリデータの解析を実施し、データをDTOへ変換し戻す。
	 *
	 * @return バイナリデータの変換結果
	 */
	public abstract T parse();

	/**
	 * 現在のポジションをチャンクの先頭として次のチャンクヘッダを読み込む。
	 */
	protected void nextChunk() {
		this.currentChunkType = readString(Chunk.CHUNK_TYPE_CODE_LENGTH);
		this.currentChunkSize = readInteger();
		this.chunkStartPosition = getPosition();
	}

	/**
	 * 現在のチャンクタイプを取得する。
	 *
	 * @return チャンクタイプ
	 */
	public String getCurrentChunkType() {
		return currentChunkType;
	}

	/**
	 * 現在のチャンクサイズを取得する。
	 *
	 * @return チャンクサイズ
	 */
	public int getCurrentChunkSize() {
		return currentChunkSize;
	}

	/**
	 * 現在のチャンクヘッダのデータをチャンクへ反映する。
	 *
	 * @param chunk
	 *            チャンクヘッダの値を反映するチャンク
	 */
	protected void populateCurrentChunkHeader(Chunk chunk) {
		chunk.setChunkType(currentChunkType);
		chunk.setChunkSize(currentChunkSize);
	}

	/**
	 * 文字列の文字セットを取得する。
	 *
	 * @return charset 文字セット
	 */
	public Charset getCharset() {
		return charset;
	}

	/**
	 * 文字列の文字セットを設定する。
	 *
	 * @param charset
	 *            文字セット
	 */
	public void setCharset(Charset charset) {
		this.charset = charset;
	}

	/**
	 * バイトを読み込む。
	 *
	 * @return バイト
	 */
	protected byte readByte() {
		return buffer.get();
	}

	/**
	 * 指定したサイズのバイト配列を読み込む
	 *
	 * @param size
	 *            バイトサイズ
	 * @return バイト配列
	 */
	protected byte[] readByte(int size) {
		int position = buffer.position();
		byte[] data = new byte[size];
		buffer.get(data, 0, size);
		buffer.position(position + size);
		return data;
	}

	/**
	 * 可変長文字列を読み込む。終端文字(0x00)までバッファを進める。
	 *
	 * @return 文字列（終端文字は含まない）
	 */
	protected String readString() {
		int firstPosition = buffer.position();
		for (byte data = buffer.get(); data != 0x00; data = buffer.get())
			;
		int lastPosition = buffer.position() - 1;

		int length = lastPosition - firstPosition;
		if (length < 1) {
			return StringUtils.EMPTY;
		}

		byte[] data = new byte[length];
		buffer.position(firstPosition);
		buffer.get(data, 0, length);
		buffer.position(lastPosition + 1);
		return new String(data, charset);
	}

	/**
	 * 固定長文字列を読み込む。
	 *
	 * @param size
	 *            バイトサイズ
	 * @return 文字列
	 */
	protected String readString(int size) {
		return new String(readByte(size), charset);
	}

	/**
	 * 論理値を読み込む。0x00であればfalse、そうでなければtrue。
	 *
	 * @return 論理地
	 */
	protected boolean readBoolean() {
		byte data = readByte();
		return data != 0x00;
	}

	/**
	 * 2バイト数値を読み込む。
	 *
	 * @return 数値
	 */
	protected short readShort() {
		return buffer.getShort();
	}

	/**
	 * 4バイト数値を読み込む。
	 *
	 * @return 数値
	 */
	protected int readInteger() {
		return buffer.getInt();
	}

	/**
	 * 指定したサイズのバイト数を<code>int</code>として読み込む。
	 *
	 * @param size
	 *            バイトサイズ
	 * @return 数値
	 */
	protected int readInteger(int size) {
		byte[] data = readByte(size);

		int value = 0;
		for (int i = 0; i < data.length; i++) {
			value = (value << 8) + (data[i] & 0xff);
		}
		return value;
	}

	/**
	 * 8バイト数値を読み込む。
	 *
	 * @return 数値
	 */
	protected long readLong() {
		return buffer.getLong();
	}

	/**
	 * 指定したサイズのバイト数を<code>long</code>として読み込む。
	 *
	 * @param size
	 *            バイトサイズ
	 * @return 数値
	 */
	protected long readLong(int size) {
		byte[] data = readByte(size);

		long value = 0;
		for (int i = 0; i < data.length; i++) {
			value = (value << 8) + (data[i] & 0xff);
		}
		return value;
	}

	/**
	 * 4バイト浮動少数を読み込む。
	 *
	 * @return 浮動少数
	 */
	protected float readFloat() {
		return buffer.getFloat();
	}

	/**
	 * 8バイト浮動少数を読み込む。
	 *
	 * @return 浮動少数
	 */
	protected double readDouble() {
		return buffer.getDouble();
	}

	/**
	 * 現在のポジション以降に未読み込みのデータが存在するか判定する。
	 *
	 * @return 未読み込みのデータが存在すればtrue
	 */
	protected boolean hasRemaining() {
		return buffer.hasRemaining();
	}

	/**
	 * 現在のポジションを取得する。
	 *
	 * @return バッファーのポジション
	 */
	protected int getPosition() {
		return buffer.position();
	}

	/**
	 * 現在のチャンクヘッダのポジションを取得する。
	 *
	 * @return 現在のチャンクヘッダの開始ポジション
	 */
	protected int getChunkStartPosition() {
		return this.chunkStartPosition;
	}

}
