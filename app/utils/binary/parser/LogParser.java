package utils.binary.parser;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import utils.binary.dto.Log;
import utils.binary.dto.chunk.CurvChunk;
import utils.binary.dto.chunk.InfoChunk;
import utils.binary.dto.chunk.LogChunk;
import utils.binary.dto.chunk.WrrChunk;
import utils.binary.exception.UnsupportedChunkTypeException;

public class LogParser extends BinaryDataParser<Log> {

	public LogParser(byte[] data) {
		super(data);
	}

	public LogParser(ByteBuffer data) {
		super(data);
	}

	static final String LOG_CHUNK_CODE = "log ";
	static final String INFO_CHUNK_CODE = "info";
	static final String CURV_CHUNK_CODE = "curv";
	static final String WRR_CHUNK_CODE = "wrr ";

	public Log parse() {
		nextChunk();
		if (!LOG_CHUNK_CODE.equals(getCurrentChunkType())) {
			throw new UnsupportedChunkTypeException(getCurrentChunkType(),
					getPosition());
		}
		LogChunk logChunk = parseLogChunk();
		populateCurrentChunkHeader(logChunk);

		List<InfoChunk> infoChunks = new ArrayList<>();
		List<CurvChunk> curvChunks = new ArrayList<>();
		List<WrrChunk> wrrChunks = new ArrayList<>();
		do {
			nextChunk();
			switch (getCurrentChunkType()) {
			case INFO_CHUNK_CODE:
				infoChunks.add(parseInfoChunk());
				break;
			case CURV_CHUNK_CODE:
				curvChunks.add(parseCurvChunk());
				break;
			case WRR_CHUNK_CODE:
				wrrChunks.add(parseWrrChunk());
				break;
			default:
				throw new UnsupportedChunkTypeException(getCurrentChunkType(),
						getPosition());
			}
		} while (hasRemaining());

		Log log = new Log();
		log.setLogChunk(logChunk);
		log.setInfoChunks(infoChunks);
		log.setCurvChunks(curvChunks);
		log.setWrrChunks(wrrChunks);
		return log;
	}

	private LogChunk parseLogChunk() {
		LogChunk chunk = new LogChunk();
		populateCurrentChunkHeader(chunk);
		chunk.setFileVersionMajor(readShort());
		chunk.setFileVersionMinor(readShort());
		chunk.setAuthor(readString());
		return chunk;
	}

	private InfoChunk parseInfoChunk() {
		InfoChunk chunk = new InfoChunk();
		populateCurrentChunkHeader(chunk);
		chunk.setUserName(readString());
		chunk.setDetaResolution(readInteger());
		chunk.setRunningStartTime(readLong());
		chunk.setRunningTime(readLong());
		chunk.setRunningDistance(readLong());
		chunk.setPointOfArrivalLattitude(readDouble());
		chunk.setPointOfArrivalLongitude(readDouble());
		chunk.setPointOfArrivalAddress(readString());
		chunk.setHighestScore(readByte());
		chunk.setAverageScore(readByte());
		return chunk;
	}

	private CurvChunk parseCurvChunk() {
		CurvChunk chunk = new CurvChunk();
		populateCurrentChunkHeader(chunk);
		chunk.setStartTime(readLong());
		chunk.setDuration(readLong());
		chunk.setPeakLattitude(readDouble());
		chunk.setPeakLongitude(readDouble());
		chunk.setEvaluation(readByte());
		chunk.setStandardPoint1(readByte());
		chunk.setStandardPoint2(readByte());
		chunk.setEvaluationScore(readByte());
		return chunk;
	}

	private WrrChunk parseWrrChunk() {
		WrrChunk chunk = new WrrChunk();
		populateCurrentChunkHeader(chunk);
		chunk.setAppRunningLogId(readString(24));
		chunk.setSnsRunningLogId(readLong());
		chunk.setBikeId(readLong());
		chunk.setRunningName(readString());
		chunk.setRunningPlace(readString());
		chunk.setComment(readString());
		chunk.setIndividualEvaluation(readByte());
		chunk.setValidCurvCount(readShort());
		chunk.setTotalScore(readInteger());
		return chunk;
	}
}
