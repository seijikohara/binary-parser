package utils.binary.exception;

public class UnsupportedChunkTypeException extends IllegalBinaryFormatException {

	private static final String MESSAGE_TEMPLATE = "Unsupported chunk type : '%1$s' at [%2$o(0x%2$x)]";

	private static String message(String chunkType, int bufferPosition) {
		return String.format(MESSAGE_TEMPLATE, chunkType, bufferPosition);
	}

	public UnsupportedChunkTypeException(String chunkType, int bufferPosition,
			Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message(chunkType, bufferPosition), cause, enableSuppression,
				writableStackTrace);
	}

	public UnsupportedChunkTypeException(String chunkType, int bufferPosition,
			Throwable cause) {
		super(message(chunkType, bufferPosition), cause);
	}

	public UnsupportedChunkTypeException(String chunkType, int bufferPosition) {
		super(message(chunkType, bufferPosition));
	}

}
