package utils.binary.exception;

public class IllegalBinaryFormatException extends RuntimeException {

	public IllegalBinaryFormatException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public IllegalBinaryFormatException(String message, Throwable cause) {
		super(message, cause);
	}

	public IllegalBinaryFormatException(String message) {
		super(message);
	}

	public IllegalBinaryFormatException(Throwable cause) {
		super(cause);
	}

}
