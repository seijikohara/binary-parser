package utils.binary.dto.chunk;

public class CurvChunk extends Chunk {

	/** 開始時刻 [msec] */
	private long startTime;

	/** 持続時間 [msec] */
	private long duration;

	/** ピーク時の緯度 */
	private double peakLattitude;

	/** ピーク時の経度 */
	private double peakLongitude;

	/** 評価 */
	private byte evaluation;

	/** 基礎点1 */
	private byte standardPoint1;

	/** 基礎点2 */
	private byte standardPoint2;

	/** 評価得点 */
	private byte evaluationScore;

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public double getPeakLattitude() {
		return peakLattitude;
	}

	public void setPeakLattitude(double peakLattitude) {
		this.peakLattitude = peakLattitude;
	}

	public double getPeakLongitude() {
		return peakLongitude;
	}

	public void setPeakLongitude(double peakLongitude) {
		this.peakLongitude = peakLongitude;
	}

	public byte getEvaluation() {
		return evaluation;
	}

	public void setEvaluation(byte evaluation) {
		this.evaluation = evaluation;
	}

	public byte getStandardPoint1() {
		return standardPoint1;
	}

	public void setStandardPoint1(byte standardPoint1) {
		this.standardPoint1 = standardPoint1;
	}

	public byte getStandardPoint2() {
		return standardPoint2;
	}

	public void setStandardPoint2(byte standardPoint2) {
		this.standardPoint2 = standardPoint2;
	}

	public byte getEvaluationScore() {
		return evaluationScore;
	}

	public void setEvaluationScore(byte evaluationScore) {
		this.evaluationScore = evaluationScore;
	}

}
