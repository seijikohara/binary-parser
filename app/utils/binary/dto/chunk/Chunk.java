package utils.binary.dto.chunk;

import utils.binary.dto.Dto;

/**
 * チャンクをあらわす基底クラス。
 * <p>
 * チャンクヘッダのデータを持つ。
 *
 * @author kohara
 *
 */
public abstract class Chunk extends Dto {

	/** チャンクヘッダにあるチャンクタイプの規定バイトサイズ */
	public static final int CHUNK_TYPE_CODE_LENGTH = 4;

	/** チャンク・タイプ */
	private String chunkType;

	/** チャンク・サイズ */
	private int chunkSize;

	public String getChunkType() {
		return chunkType;
	}

	public void setChunkType(String chunkType) {
		this.chunkType = chunkType;
	}

	public int getChunkSize() {
		return chunkSize;
	}

	public void setChunkSize(int chunkSize) {
		this.chunkSize = chunkSize;
	}

}
