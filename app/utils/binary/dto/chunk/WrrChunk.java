package utils.binary.dto.chunk;

public class WrrChunk extends Chunk {

	/** 走行ログID (アプリ) */
	private String appRunningLogId;

	/** 走行ログID (SNS) */
	private long snsRunningLogId;

	/** バイクID */
	private long bikeId;

	/** 走行名 */
	private String runningName;

	/** 走行場所 */
	private String runningPlace;

	/** コメント */
	private String comment;

	/** 個人評価 */
	private byte individualEvaluation;

	/** 有効カーブ数 */
	private short validCurvCount;

	/** 合計得点 */
	private int totalScore;

	public String getAppRunningLogId() {
		return appRunningLogId;
	}

	public void setAppRunningLogId(String appRunningLogId) {
		this.appRunningLogId = appRunningLogId;
	}

	public long getSnsRunningLogId() {
		return snsRunningLogId;
	}

	public void setSnsRunningLogId(long snsRunningLogId) {
		this.snsRunningLogId = snsRunningLogId;
	}

	public long getBikeId() {
		return bikeId;
	}

	public void setBikeId(long bikeId) {
		this.bikeId = bikeId;
	}

	public String getRunningName() {
		return runningName;
	}

	public void setRunningName(String runningName) {
		this.runningName = runningName;
	}

	public String getRunningPlace() {
		return runningPlace;
	}

	public void setRunningPlace(String runningPlace) {
		this.runningPlace = runningPlace;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public byte getIndividualEvaluation() {
		return individualEvaluation;
	}

	public void setIndividualEvaluation(byte individualEvaluation) {
		this.individualEvaluation = individualEvaluation;
	}

	public short getValidCurvCount() {
		return validCurvCount;
	}

	public void setValidCurvCount(short validCurvCount) {
		this.validCurvCount = validCurvCount;
	}

	public int getTotalScore() {
		return totalScore;
	}

	public void setTotalScore(int totalScore) {
		this.totalScore = totalScore;
	}

}
