package utils.binary.dto.chunk;

public class InfoChunk extends Chunk {

	/** ユーザー名 */
	private String userName;

	/** データの分解能[msec] */
	private int detaResolution;

	/** 走行開始時刻 [msec] */
	private long runningStartTime;

	/** 走行時間 [msec] */
	private long runningTime;

	/** 走行距離 [m] */
	private long runningDistance;

	/** 到着地点 (緯度) */
	private double pointOfArrivalLattitude;

	/** 到着地点 (経度) */
	private double pointOfArrivalLongitude;

	/** 到着地点 (住所) */
	private String pointOfArrivalAddress;

	/** 最高得点 */
	private byte highestScore;

	/** 平均得点 */
	private byte averageScore;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getDetaResolution() {
		return detaResolution;
	}

	public void setDetaResolution(int detaResolution) {
		this.detaResolution = detaResolution;
	}

	public long getRunningStartTime() {
		return runningStartTime;
	}

	public void setRunningStartTime(long runningStartTime) {
		this.runningStartTime = runningStartTime;
	}

	public long getRunningTime() {
		return runningTime;
	}

	public void setRunningTime(long runningTime) {
		this.runningTime = runningTime;
	}

	public long getRunningDistance() {
		return runningDistance;
	}

	public void setRunningDistance(long runningDistance) {
		this.runningDistance = runningDistance;
	}

	public double getPointOfArrivalLattitude() {
		return pointOfArrivalLattitude;
	}

	public void setPointOfArrivalLattitude(double pointOfArrivalLattitude) {
		this.pointOfArrivalLattitude = pointOfArrivalLattitude;
	}

	public double getPointOfArrivalLongitude() {
		return pointOfArrivalLongitude;
	}

	public void setPointOfArrivalLongitude(double pointOfArrivalLongitude) {
		this.pointOfArrivalLongitude = pointOfArrivalLongitude;
	}

	public String getPointOfArrivalAddress() {
		return pointOfArrivalAddress;
	}

	public void setPointOfArrivalAddress(String pointOfArrivalAddress) {
		this.pointOfArrivalAddress = pointOfArrivalAddress;
	}

	public byte getHighestScore() {
		return highestScore;
	}

	public void setHighestScore(byte highestScore) {
		this.highestScore = highestScore;
	}

	public byte getAverageScore() {
		return averageScore;
	}

	public void setAverageScore(byte averageScore) {
		this.averageScore = averageScore;
	}

}
