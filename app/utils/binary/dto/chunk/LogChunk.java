package utils.binary.dto.chunk;

public class LogChunk extends Chunk {

	/** ファイル・バージョン */
	private short fileVersionMajor;

	/** ファイル・バージョン */
	private short fileVersionMinor;

	/** 作成者 (アプリ名) */
	private String author;

	public short getFileVersionMajor() {
		return fileVersionMajor;
	}

	public void setFileVersionMajor(short fileVersionMajor) {
		this.fileVersionMajor = fileVersionMajor;
	}

	public short getFileVersionMinor() {
		return fileVersionMinor;
	}

	public void setFileVersionMinor(short fileVersionMinor) {
		this.fileVersionMinor = fileVersionMinor;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

}
