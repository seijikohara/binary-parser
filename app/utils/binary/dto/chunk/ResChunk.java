package utils.binary.dto.chunk;

import java.util.List;

public class ResChunk extends Chunk {

	private List<ResData> resDatas;

	public List<ResData> getResDatas() {
		return resDatas;
	}

	public void setResDatas(List<ResData> resDatas) {
		this.resDatas = resDatas;
	}

	public static class ResData {

		/** データの無効フラグ */
		private boolean invalidFlag;

		/** GPS緯度 */
		private double gpsLatitude;

		/** GPS経度 */
		private double gpsLongitude;

		/** GPSバンク角 */
		private float gpsBankAngle;

		/** GPS加速度 */
		private float gpsAcceleration;

		public boolean isInvalidFlag() {
			return invalidFlag;
		}

		public void setInvalidFlag(boolean invalidFlag) {
			this.invalidFlag = invalidFlag;
		}

		public double getGpsLatitude() {
			return gpsLatitude;
		}

		public void setGpsLatitude(double gpsLatitude) {
			this.gpsLatitude = gpsLatitude;
		}

		public double getGpsLongitude() {
			return gpsLongitude;
		}

		public void setGpsLongitude(double gpsLongitude) {
			this.gpsLongitude = gpsLongitude;
		}

		public float getGpsBankAngle() {
			return gpsBankAngle;
		}

		public void setGpsBankAngle(float gpsBankAngle) {
			this.gpsBankAngle = gpsBankAngle;
		}

		public float getGpsAcceleration() {
			return gpsAcceleration;
		}

		public void setGpsAcceleration(float gpsAcceleration) {
			this.gpsAcceleration = gpsAcceleration;
		}

	}

}
