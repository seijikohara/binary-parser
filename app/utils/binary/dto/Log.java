package utils.binary.dto;

import java.util.List;

import utils.binary.dto.chunk.CurvChunk;
import utils.binary.dto.chunk.InfoChunk;
import utils.binary.dto.chunk.LogChunk;
import utils.binary.dto.chunk.WrrChunk;

/**
 * Logファイルのデータを保持する。
 * <p>
 * Logファイルは、走行結果やカーブデータを保持する。
 *
 * @author kohara
 *
 */
public class Log extends Dto {

	private LogChunk logChunk;

	private List<InfoChunk> infoChunks;

	private List<CurvChunk> curvChunks;

	private List<WrrChunk> wrrChunks;

	public LogChunk getLogChunk() {
		return logChunk;
	}

	public void setLogChunk(LogChunk logChunk) {
		this.logChunk = logChunk;
	}

	public List<InfoChunk> getInfoChunks() {
		return infoChunks;
	}

	public void setInfoChunks(List<InfoChunk> infoChunks) {
		this.infoChunks = infoChunks;
	}

	public List<CurvChunk> getCurvChunks() {
		return curvChunks;
	}

	public void setCurvChunks(List<CurvChunk> curvChunks) {
		this.curvChunks = curvChunks;
	}

	public List<WrrChunk> getWrrChunks() {
		return wrrChunks;
	}

	public void setWrrChunks(List<WrrChunk> wrrChunks) {
		this.wrrChunks = wrrChunks;
	}

}
