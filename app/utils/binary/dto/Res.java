package utils.binary.dto;

import utils.binary.dto.chunk.ResChunk;

/**
 * Resファイルのデータを保持する。
 * <p>
 * Resファイルは、Logファイルに明記した分解能間隔のGPS座標値と走行姿勢を保持する。
 *
 * @author kohara
 *
 */
public class Res extends Dto {

	private ResChunk resChunk;

	public ResChunk getResChunk() {
		return resChunk;
	}

	public void setResChunk(ResChunk resChunk) {
		this.resChunk = resChunk;
	}

}
