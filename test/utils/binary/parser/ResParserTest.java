/**
 *
 */
package utils.binary.parser;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.nio.ByteBuffer;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import utils.binary.dto.Res;

/**
 * @author kohara
 *
 */
public class ResParserTest implements ParserTestHelper {

	private static final int CHUNK_HEADER_SIZE = 8; // チャンク・タイプ + チャンク・サイズ
	private static final int RES_CHUNK_DATA_SIZE = 25; // データのサイズ

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * {@link utils.binary.parser.ResParser#ResParser(byte[])} のためのテスト・メソッド。
	 */
	@Test
	public void testResParserByteArray() {
		byte[] data = generateResData(3).array();

		ResParser testee = new ResParser(data);
		Res actual = testee.parse();

		assertThat(actual, is(not(nullValue())));
	}

	/**
	 * {@link utils.binary.parser.ResParser#ResParser(java.nio.ByteBuffer)}
	 * のためのテスト・メソッド。
	 */
	@Test
	public void testResParserByteBuffer() {
		ByteBuffer data = generateResData(3);

		ResParser testee = new ResParser(data);
		Res actual = testee.parse();

		assertThat(actual, is(not(nullValue())));
	}

	/**
	 * {@link utils.binary.parser.ResParser#parse()} のためのテスト・メソッド。
	 */
	@Test
	public void testParse() {
		fail("まだ実装されていません");
	}

	private ByteBuffer generateResData(int dataCount) {
		ByteBuffer buffer = ByteBuffer.allocate(RES_CHUNK_DATA_SIZE * dataCount
				+ CHUNK_HEADER_SIZE);

		// チャンクヘッダー構築
		buffer.put("res ".getBytes());
		buffer.putInt(RES_CHUNK_DATA_SIZE * dataCount);

		// チャンクデータ構築
		for (int i = 0; i < dataCount; i++) {
			buffer.put((byte) 1);
			buffer.putDouble(dataCount * 10.1d);
			buffer.putDouble(dataCount * 20.2d);
			buffer.putFloat(dataCount * 30.3f);
			buffer.putFloat(dataCount * 40.4f);
		}

		buffer.rewind();

		return buffer;
	}
}
