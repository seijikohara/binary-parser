package utils.binary.parser;

import java.nio.ByteBuffer;

import org.apache.commons.lang3.StringUtils;

public interface ParserTestHelper {

	/**
	 * 可変長文字列のデータサイズ（バイト数）を取得する。
	 *
	 * @param text
	 *            データサイズを求める文字列
	 * @return 終端文字を含むデータサイズ
	 */
	default int getVariableTextSize(String text) {
		return getFixedTextSize(text) + 1;
	}

	/**
	 * 固定長文字列のデータサイズ（バイト数）を取得する。
	 *
	 * @param text
	 *            データサイズを求める文字列
	 * @return データサイズ
	 */
	default int getFixedTextSize(String text) {
		return (StringUtils.isEmpty(text) ? 0 : text.getBytes().length);
	}

	/**
	 * 可変長文字列をバイナリ表現に変換する。終端文字列が付与される。
	 *
	 * @param text
	 *            バイナリ表現に変換する文字列
	 * @return 終端文字を含む文字列のバイナリ表現
	 */
	default byte[] toBinaryVariableText(String text) {
		byte[] data = StringUtils.isEmpty(text) ? new byte[0] : text.getBytes();
		ByteBuffer buffer = ByteBuffer.allocate(data.length + 1);
		buffer.put(data);
		buffer.put((byte) 0x00); // 終端文字をセット
		return buffer.array();
	}

	/**
	 * 固定長文字を付与したバイナリ表現に変換する。
	 *
	 * @param text
	 *            バイナリ表現に変換する文字列
	 * @return 終端文字を含む文字列のバイナリ表現
	 */
	default byte[] toBinaryFixedText(String text) {
		byte[] data = StringUtils.isEmpty(text) ? new byte[0] : text.getBytes();
		ByteBuffer buffer = ByteBuffer.wrap(data);
		return buffer.array();
	}

}
