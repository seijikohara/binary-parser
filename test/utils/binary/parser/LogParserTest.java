/**
 *
 */
package utils.binary.parser;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.io.File;
import java.nio.ByteBuffer;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import utils.binary.dto.Log;
import utils.binary.dto.chunk.CurvChunk;
import utils.binary.dto.chunk.InfoChunk;
import utils.binary.dto.chunk.LogChunk;
import utils.binary.dto.chunk.WrrChunk;

/**
 * @author kohara
 *
 */
public class LogParserTest implements ParserTestHelper {

	static final int CHUNK_HEADER_SIZE = 8; // チャンク・タイプ + チャンク・サイズ

	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * {@link utils.binary.parser.LogParser#LogParser(byte[])} のためのテスト・メソッド。
	 */
	@Test
	public void testLogParserByteArray() {
		fail("まだ実装されていません");
	}

	/**
	 * {@link utils.binary.parser.LogParser#LogParser(java.nio.ByteBuffer)}
	 * のためのテスト・メソッド。
	 */
	@Test
	public void testLogParserByteBuffer() {
		fail("まだ実装されていません");
	}

	/**
	 * {@link utils.binary.parser.LogParser#parse()} のためのテスト・メソッド。
	 */
	@Test
	public void testParse() throws Exception {
		LogChunk logChunk = createLogChunk();
		byte[] logData = toBinary(logChunk);

		InfoChunk infoChunk = createInfoChunk();
		byte[] infoData = toBinary(infoChunk);

		CurvChunk curvChunk = createCurvChunk();
		byte[] curvData = toBinary(curvChunk);

		WrrChunk wrrChunk = createWrrChunk();
		byte[] wrrData = toBinary(wrrChunk);

		ByteBuffer buffer = createByteBuffer(logData, infoData, curvData,
				wrrData, infoData, wrrData, wrrData);

		File temporaryFile = temporaryFolder.newFile("test");
		FileUtils.writeByteArrayToFile(temporaryFile, buffer.array());

		LogParser parser = new LogParser(buffer);

		Log actual = parser.parse();

		assertThat(actual, is(not(nullValue())));
		assertThat(actual.getLogChunk(), is(logChunk));
		assertThat(actual.getInfoChunks().size(), is(2));
		assertThat(actual.getInfoChunks().get(0), is(infoChunk));
		assertThat(actual.getInfoChunks().get(1), is(infoChunk));
		assertThat(actual.getCurvChunks().size(), is(1));
		assertThat(actual.getCurvChunks().get(0), is(curvChunk));
		assertThat(actual.getWrrChunks().size(), is(3));
		assertThat(actual.getWrrChunks().get(0), is(wrrChunk));
		assertThat(actual.getWrrChunks().get(1), is(wrrChunk));
		assertThat(actual.getWrrChunks().get(2), is(wrrChunk));
	}

	private LogChunk createLogChunk() {
		LogChunk logChunk = new LogChunk();
		logChunk.setChunkType("log ");
		logChunk.setChunkSize(29);
		logChunk.setFileVersionMajor((short) 1); // 2
		logChunk.setFileVersionMinor((short) 2); // 2
		logChunk.setAuthor("作成者 (アプリ名)"); // 25
		return logChunk;
	}

	private InfoChunk createInfoChunk() {
		InfoChunk infoChunk = new InfoChunk();
		infoChunk.setChunkType("info");
		infoChunk.setChunkSize(83);
		infoChunk.setUserName("ユーザー名"); // 16
		infoChunk.setDetaResolution(1); // 4
		infoChunk.setRunningStartTime(2L); // 8
		infoChunk.setRunningTime(3L); // 8
		infoChunk.setRunningDistance(4L); // 8
		infoChunk.setPointOfArrivalLattitude(5.5d); // 8
		infoChunk.setPointOfArrivalLongitude(6.6d); // 8
		infoChunk.setPointOfArrivalAddress("到着地点(住所)"); // 21
		infoChunk.setHighestScore((byte) 7); // 1
		infoChunk.setAverageScore((byte) 8); // 1
		return infoChunk;
	}

	private CurvChunk createCurvChunk() {
		CurvChunk curvChunk = new CurvChunk();
		curvChunk.setChunkType("curv");
		curvChunk.setChunkSize(36);
		curvChunk.setStartTime(1L); // 8
		curvChunk.setDuration(2L); // 8
		curvChunk.setPeakLattitude(3.3d); // 8
		curvChunk.setPeakLongitude(4.4d); // 8
		curvChunk.setEvaluation((byte) 5); // 1
		curvChunk.setStandardPoint1((byte) 6); // 1
		curvChunk.setStandardPoint2((byte) 7); // 1
		curvChunk.setEvaluationScore((byte) 8); // 1
		return curvChunk;
	}

	private WrrChunk createWrrChunk() {
		WrrChunk wrrChunk = new WrrChunk();
		wrrChunk.setChunkType("wrr ");
		wrrChunk.setChunkSize(83);
		wrrChunk.setAppRunningLogId("123456789012345678901234"); // 24バイト固定
		wrrChunk.setSnsRunningLogId(1L); // 8
		wrrChunk.setBikeId(2L); // 8
		wrrChunk.setRunningName("走行名"); // 10
		wrrChunk.setRunningPlace("走行場所"); // 13
		wrrChunk.setComment("コメント"); // 13
		wrrChunk.setIndividualEvaluation((byte) 3); // 1
		wrrChunk.setValidCurvCount((short) 4); // 2
		wrrChunk.setTotalScore(5); // 4
		return wrrChunk;
	}

	private ByteBuffer createByteBuffer(byte[]... datas) {
		int capacity = 0;
		for (byte[] data : datas) {
			capacity += data.length;
		}
		ByteBuffer buffer = ByteBuffer.allocate(capacity);
		for (byte[] data : datas) {
			buffer.put(data);
		}
		buffer.rewind();
		return buffer;
	}

	private byte[] toBinary(LogChunk chunk) {
		int chunkSize = Short.BYTES + Short.BYTES
				+ getVariableTextSize(chunk.getAuthor());
		ByteBuffer buffer = ByteBuffer.allocate(CHUNK_HEADER_SIZE + chunkSize);

		buffer.put("log ".getBytes());
		buffer.putInt(chunkSize);
		buffer.putShort(chunk.getFileVersionMajor());
		buffer.putShort(chunk.getFileVersionMinor());
		buffer.put(toBinaryVariableText(chunk.getAuthor()));

		return buffer.array();
	}

	private byte[] toBinary(InfoChunk chunk) {
		int chunkSize = getVariableTextSize(chunk.getUserName())
				+ Integer.BYTES + Long.BYTES + Long.BYTES + Long.BYTES
				+ Double.BYTES + Double.BYTES
				+ getVariableTextSize(chunk.getPointOfArrivalAddress())
				+ Byte.BYTES + Byte.BYTES;
		ByteBuffer buffer = ByteBuffer.allocate(CHUNK_HEADER_SIZE + chunkSize);

		buffer.put("info".getBytes());
		buffer.putInt(chunkSize);
		buffer.put(toBinaryVariableText(chunk.getUserName()));
		buffer.putInt(chunk.getDetaResolution());
		buffer.putLong(chunk.getRunningStartTime());
		buffer.putLong(chunk.getRunningTime());
		buffer.putLong(chunk.getRunningDistance());
		buffer.putDouble(chunk.getPointOfArrivalLattitude());
		buffer.putDouble(chunk.getPointOfArrivalLongitude());
		buffer.put(toBinaryVariableText(chunk.getPointOfArrivalAddress()));
		buffer.put(chunk.getHighestScore());
		buffer.put(chunk.getAverageScore());

		return buffer.array();
	}

	private byte[] toBinary(CurvChunk chunk) {
		int chunkSize = Long.BYTES + Long.BYTES + Double.BYTES + Double.BYTES
				+ Byte.BYTES + Byte.BYTES + Byte.BYTES + Byte.BYTES;
		ByteBuffer buffer = ByteBuffer.allocate(CHUNK_HEADER_SIZE + chunkSize);

		buffer.put("curv".getBytes());
		buffer.putInt(chunkSize);
		buffer.putLong(chunk.getStartTime());
		buffer.putLong(chunk.getDuration());
		buffer.putDouble(chunk.getPeakLattitude());
		buffer.putDouble(chunk.getPeakLongitude());
		buffer.put(chunk.getEvaluation());
		buffer.put(chunk.getStandardPoint1());
		buffer.put(chunk.getStandardPoint2());
		buffer.put(chunk.getEvaluationScore());

		return buffer.array();
	}

	private byte[] toBinary(WrrChunk chunk) {
		int runningNameSize = getVariableTextSize(chunk.getRunningName());
		int runningPlaceSize = getVariableTextSize(chunk.getRunningPlace());
		int commentSize = getVariableTextSize(chunk.getComment());

		int chunkSize = 24 + Long.BYTES + Long.BYTES + runningNameSize
				+ runningPlaceSize + commentSize + Byte.BYTES + Short.BYTES
				+ Integer.BYTES;
		ByteBuffer buffer = ByteBuffer.allocate(CHUNK_HEADER_SIZE + chunkSize);

		buffer.put("wrr ".getBytes());
		buffer.putInt(chunkSize);
		buffer.put(toBinaryFixedText(chunk.getAppRunningLogId()));
		buffer.putLong(chunk.getSnsRunningLogId());
		buffer.putLong(chunk.getBikeId());
		buffer.put(toBinaryVariableText(chunk.getRunningName()));
		buffer.put(toBinaryVariableText(chunk.getRunningPlace()));
		buffer.put(toBinaryVariableText(chunk.getComment()));
		buffer.put(chunk.getIndividualEvaluation());
		buffer.putShort(chunk.getValidCurvCount());
		buffer.putInt(chunk.getTotalScore());

		return buffer.array();
	}
}
